
#include "../stack.h"


#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdlib.h>
#include <stdio.h>


static void test_add_int_to_stack(void **state)
{
    const size_t num = 10000;
    stack_t stack = *stack_init();
    int a[num];

    for (int itt = 0; itt < num; ++itt) {
        a[itt] = itt;
        stack.push(&stack, &a[itt]);
    }
    assert_int_equal(stack.size(&stack), num);
    stack.delete(&stack);
}

static void test_elem_equal(void **state)
{
    const size_t num = 10000;
    stack_t stack = *stack_init();
    int a[num];
    int b;
    for (int itt = 0; itt < num; ++itt) {
        a[itt] = itt;
        stack.push(&stack, &a[itt]);
    }
    stack.at(&stack, &b, 0);
    assert_int_equal(b, 0);

    stack.at(&stack, &b, num-1);
    assert_int_equal(b, num-1);
    stack.delete(&stack);
}

static void test_ptr_test(void **state)
{
    const size_t num = 10000;

    stack_t stack = *stack_init();
    int a[num];// = malloc(num * sizeof(size_t));
    int i;

    for (i = 0; i < num; ++i) {
        a[i] = i;
        stack.push(&stack, &a[i]);
    }

    int *b = malloc(sizeof(int));

    for (i = num-1; i >= 0; --i) {
        stack.pop(&stack, b);

        assert_int_equal(*b, i);
    }

    stack.delete(&stack);

}

static void test_struct(void **state)
{
    struct Test {
        int i;
        float f;
    };
    stack_t stack = *stack_init();
    const int num = 1000;
    struct Test test[num];

    for (int itt = 0; itt < num; ++itt) {
        test[itt].i = itt;
        stack.push(&stack, &test[itt]);
    }

    assert_int_equal(test[0].i, 0);
    assert_int_equal(test[num-1].i, num-1);

    int i[num];
    for (int itt = num - 1; itt >= 0; --itt) {
        stack.pop(&stack, &i[itt]);
        assert_int_equal(i[itt], itt);
    }

    stack.delete(&stack);
}


int main()
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_add_int_to_stack),
        cmocka_unit_test(test_elem_equal),
        cmocka_unit_test(test_ptr_test),
        cmocka_unit_test(test_struct),
    };

    cmocka_run_group_tests(tests, NULL, NULL);

}