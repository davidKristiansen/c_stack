/* stack.c -- Source file for stack class
 *
 * Copyright (C) <2017> <David Kristiansen>
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD license.  See the LICENSE file for details.
 */

#include "stack.h"

typedef struct stack_t_impl stack_t_impl;

int stack_push(stack_t *self, void *data);
int stack_pop(stack_t *self, void *data);
int stack_at(stack_t *self, void *data, unsigned int pos);
int stack_delete(stack_t *self);
size_t stack_size(stack_t *self);

struct stack_t_impl
{
    void **data;
    size_t capacity;
    size_t size;
};

int stack_push(stack_t *self, void *data)
{
    stack_t_impl *impl = self->impl;

    if (impl->size >= impl->capacity) {
        impl->data = (void **) realloc(impl->data, (impl->capacity *= 2) * sizeof(void *));
        if (!impl->data) { return -1; }
    }

    impl->data[impl->size++] = data;
    return 0;
}

int stack_pop(stack_t *self, void *data)
{
    stack_t_impl *impl = self->impl;

    if (impl->size <= 0) { return -1; }

    if (--impl->size < impl->capacity / 4) {
        impl->data = (void **) realloc(impl->data, (impl->capacity /= 2) * sizeof(void *));
        if (!impl->data) { return -1; }
    }

    *(void **) data = *(void **) (impl->data[impl->size]);

    return 0;
}

int stack_at(stack_t *self, void *data, unsigned int pos)
{
    stack_t_impl *impl = self->impl;

    if (impl->size <= 0 || pos >= impl->size) { return -1; }

    *(void **) data = *(void **) impl->data[pos];

    return 0;

}

int stack_delete(stack_t *self)
{
    stack_t_impl *impl = self->impl;

    free(impl->data);
    if (!impl->data) { return -1; }
    free(impl);
    if (!self->impl) { return -1; }
    return 0;

}

size_t stack_size(stack_t *self)
{
    stack_t_impl *impl = self->impl;

    return impl->size;
}

stack_t *stack_init()
{
    stack_t *self = malloc(sizeof(stack_t));
    self->impl = malloc(sizeof(stack_t_impl));

    stack_t_impl *impl = self->impl;

    impl->data = malloc(sizeof(void *));

    self->push = stack_push;
    self->pop = stack_pop;
    self->at = stack_at;
    self->delete = stack_delete;
    self->size = stack_size;

    impl->size = 0;
    impl->capacity = 1;

    return self;
}