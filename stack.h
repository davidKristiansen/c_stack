/* stack.h -- Header file for stack class
 *
 * Copyright (C) <2017> <David Kristiansen>
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD license.  See the LICENSE file for details.
 */
#pragma once

#include <stdlib.h>

typedef struct stack_t stack_t;

struct stack_t
{
    int (*push)(stack_t *self, void *data);
    int (*pop)(stack_t *self, void *data);
    int (*at)(stack_t *self, void *data, unsigned int pos);
    int (*delete)(stack_t *self);
    size_t (*size)(stack_t *self);

    void *impl;
};

stack_t *stack_init();